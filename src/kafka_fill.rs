use crate::data::{conf::Conf, taxiarrow::TaxiArrow, taxientry::TaxiEntry};
use anyhow::Result;
use arrow2::{datatypes::Field, io::parquet::read::FileReader};
use rskafka::{
    client::{partition::Compression, ClientBuilder},
    record::Record,
    time::OffsetDateTime,
};
use std::path::Path;
use std::{collections::BTreeMap, fs::File};
use tracing::debug;

/// Pushes all given Taxi Entry values in path as parquet to a kafka topic encoded with Bincode.
///
/// # Panics
///
/// - the topic already exist and is not None.
/// - The given kafka host is not reachable.
/// -
///
pub async fn fill_kafka(conf: &Conf) -> Result<()> {
    // setup client
    let connection = conf.output_uri.to_owned();
    debug!("setting kafka connection uri: {:?}", connection);
    let client = ClientBuilder::new(vec![connection]).build().await?;
    debug!("Created a kafka client: {:?}", client);

    // create a topic
    if conf.create_topic {
        let controller_client = client.controller_client().await?;
        controller_client
            .create_topic(
                &conf.topic,
                1,     // partitions
                1,     // replication factor
                5_000, // timeout (ms)
            )
            .await?;
    }

    // get a partition-bound client
    let partition_client = client
        .partition_client(
            conf.topic.to_owned(),
            0, // partition
        )
        .await?;

    // read the data file
    let path = Path::new(&conf.data_path);
    debug!("path to parquet file: {:?}", path);

    let file = File::open(path)?;
    debug!("openend reader: {:?}", file);
    let projection: Vec<usize> = (0..17).collect();

    let reader = FileReader::try_new(
        file,
        Some(&projection),
        Some(conf.batch_size),
        Some(conf.limit),
        None,
    )?;

    let fields: Vec<Field> = reader.schema().fields.clone();

    debug!("Fields of the parquet data: \n{:#?}", fields);

    for (_i, maybe_chunk) in reader.enumerate() {
        let mut records = Vec::new();
        let chunk = maybe_chunk?;
        let ta = TaxiArrow::new(chunk);
        // debug!("{i}: {:#?}", ta);
        let te: Vec<TaxiEntry> = ta.into();
        // debug!("{i}: {:#?}", te);
        te.into_iter().for_each(|t| {
            let data = rkyv::to_bytes::<_, 1024>(&t).unwrap();
            let record = Record {
                key: None,
                value: Some(data.to_vec()),
                headers: BTreeMap::from([]),
                timestamp: OffsetDateTime::from_unix_timestamp_nanos(
                    t.pickup_datetime.unwrap() as i128
                )
                .unwrap(),
            };
            records.push(record);
        });
        partition_client
            .produce(records, Compression::default())
            .await
            .unwrap();
    }

    debug!("done iterating data");

    Ok(())
}

use std::time::Duration;

use crate::aggregator::aggrows;
use crate::data::conf::Purpose;
use crate::kafka_fill::fill_kafka;
use data::{conf::Conf, sendable_durr::Durr};

use anyhow::Result;
use futures::SinkExt;
use tokio::{io::Interest, net::TcpStream};
use tokio_util::codec::{Framed, LengthDelimitedCodec};

pub mod data;
pub mod kafka_fill;
pub mod sinks;

mod aggregator;
mod runner;

pub async fn run(conf: Conf) -> Result<()> {
    match &conf.purpose {
        Purpose::Aggregator => aggrows(conf).await?,
        Purpose::Kafka => fill_kafka(&conf).await?,
        Purpose::Runner => runner::run(&conf).await?,
        Purpose::TimeSink => sinks::aggregate_sink(conf.output_uri.to_owned()).await?,
    }
    Ok(())
}

pub async fn send_durration(conf: &Conf, durr: Duration) -> Result<()> {
    let time_uri = conf.time_uri.to_owned();
    let stream = TcpStream::connect(time_uri).await?;
    let codec = LengthDelimitedCodec::new();
    stream.ready(Interest::WRITABLE).await?;
    let mut transport = Framed::new(stream, codec);
    let durr = Durr { durr };
    let data = rkyv::to_bytes::<_, 1024>(&durr).unwrap().to_vec().into();

    transport.send(data).await?;

    Ok(())
}

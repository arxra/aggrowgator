use clap::{Parser, Subcommand};

/// The modes the program can run in.
/// Normal data-flow graph:
/// ```no_test
/// [Benchmark Program] -> [Aggregator] -> Runner
///                ^\   \_________________/^     \
///                  \___________________________/
/// ```
///
#[derive(Subcommand, Debug)]
pub enum Purpose {
    /// Data reader which pushes it to output URI. Requires output URI
    Runner,
    /// The client doing the aggregation. Requires input and output URI's
    Aggregator,
    /// Send data to kafka topic. Requires Output URI
    Kafka,
    /// Starts a reciveing port which dropps incoming data as time.
    TimeSink,
}

impl Default for Purpose {
    fn default() -> Self {
        Self::Runner
    }
}

impl std::str::FromStr for Purpose {
    type Err = std::io::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "runner" | "Runner" => Ok(Self::Runner),
            "Agg" | "agg" | "Aggregator" | "aggregator" => Ok(Self::Aggregator),
            "kafka" | "Kafka" => Ok(Self::Kafka),
            "sink" | "timesink" | "ts" => Ok(Self::TimeSink),
            _ => Err(std::io::Error::new(
                std::io::ErrorKind::InvalidInput,
                "No such purpose",
            )),
        }
    }
}

impl std::fmt::Display for Purpose {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// Bencmarking program for looking at how arrow can be used in Global Window Aggregations,
/// as part of the authors masters thesis.
#[derive(Parser, Debug, Default)]
pub struct Conf {
    /// Number of times to greet
    #[clap(short, long, default_value_t = Purpose::Aggregator)]
    pub(crate) purpose: Purpose,
    /// The connection url for outgoing data
    #[clap(short, long, default_value = "localhost:9091")]
    pub(crate) output_uri: String,
    /// The connection url for incoming data. Will open a socket
    #[clap(short, long, default_value = "localhost:9092")]
    pub(crate) input_uri: String,
    /// The connection url for durrations, used to benchmark. Will open a socket
    #[clap(long, default_value = "localhost:9093")]
    pub(crate) time_uri: String,
    /// Topic name in kafka
    #[clap(short, long, default_value = "my_topic")]
    pub(crate) topic: String,
    /// wether to create the topic in kafka or assume present
    #[clap(short, long)]
    pub(crate) create_topic: bool,
    /// Path to a parquet file of taxi entries.
    #[clap(short, long, default_value = "")]
    pub(crate) data_path: String,
    /// Batch size to use in the application.
    #[clap(short, long, default_value = "1048")]
    pub(crate) batch_size: usize,
    /// The limit of total elements to use.
    #[clap(short, long, default_value = "1000000")]
    pub(crate) limit: usize,
}

impl Conf {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        purpose: Purpose,
        output_uri: String,
        input_uri: String,
        runner_uri: String,
        topic: String,
        create_topic: bool,
        data_path: String,
        batch_size: usize,
        limit: usize,
    ) -> Self {
        Self {
            purpose,
            output_uri,
            input_uri,
            time_uri: runner_uri,
            topic,
            create_topic,
            data_path,
            batch_size,
            limit,
        }
    }

    pub fn set_batch_size(&mut self, batch_size: usize) {
        self.batch_size = batch_size;
    }
}

use std::time::Duration;

use rkyv::{Archive, Deserialize, Serialize};

#[derive(Deserialize, Serialize, PartialEq, Debug, Archive, Default, Clone)]
pub struct Durr {
    pub durr: Duration,
}

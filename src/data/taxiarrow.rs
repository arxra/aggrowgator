use std::{fs::File, path::Path, sync::Arc};

use arrow2::{
    array::{Array, PrimitiveArray, Utf8Array},
    chunk::Chunk,
    io::parquet::read::FileReader,
};
use tracing::debug;

use super::conf::Conf;

// If we are going to typecast through `any` we might as well use macros

macro_rules! to_arrow_utf8 {
    // Just to make it pretty down there....
    ($array:expr) => {
        $array
            .next()
            .unwrap()
            .as_any()
            .downcast_ref::<Utf8Array<i32>>()
            .unwrap()
            .to_owned()
    };
}
macro_rules! to_arrow {
    ($type:ty, $array:expr) => {
        $array
            .next()
            .unwrap()
            .as_any()
            .downcast_ref::<PrimitiveArray<$type>>()
            .unwrap()
            .to_owned()
    };
}

#[derive(Debug, PartialEq)]
pub struct TaxiArrow {
    len: usize,
    pub vendor_id: PrimitiveArray<i64>,
    pub pickup_datetime: PrimitiveArray<i64>,
    pub dropoff_datetime: PrimitiveArray<i64>,
    pub passenger_count: PrimitiveArray<f64>,
    pub trip_distance: PrimitiveArray<f64>,
    pub ratecode_id: PrimitiveArray<f64>,
    pub store_and_fwd_flag: Utf8Array<i32>,
    pub pu_location_ic: PrimitiveArray<i64>,
    pub do_location_ic: PrimitiveArray<i64>,
    pub payment_type: PrimitiveArray<i64>,
    pub fare_amount: PrimitiveArray<f64>,
    pub extra: PrimitiveArray<f64>,
    pub mta_tax: PrimitiveArray<f64>,
    pub tip_amount: PrimitiveArray<f64>,
    pub tolls_amount: PrimitiveArray<f64>,
    pub improvement_surcharge: PrimitiveArray<f64>,
    pub total_amount: PrimitiveArray<f64>,
}

impl TaxiArrow {
    pub fn new(chunk: Chunk<Arc<dyn Array>>) -> Self {
        let len = chunk.len();

        let mut arrays = chunk.into_arrays().into_iter();

        let vendor_id = to_arrow!(i64, arrays);
        let pickup_datetime = to_arrow!(i64, arrays);
        let dropoff_datetime = to_arrow!(i64, arrays);
        let passenger_count = to_arrow!(f64, arrays);
        let trip_distance = to_arrow!(f64, arrays);
        let ratecode_id = to_arrow!(f64, arrays);
        let store_and_fwd_flag = to_arrow_utf8!(arrays);
        let pu_location_ic = to_arrow!(i64, arrays);
        let do_location_ic = to_arrow!(i64, arrays);
        let payment_type = to_arrow!(i64, arrays);
        let fare_amount = to_arrow!(f64, arrays);
        let extra = to_arrow!(f64, arrays);
        let mta_tax = to_arrow!(f64, arrays);
        let tip_amount = to_arrow!(f64, arrays);
        let tolls_amount = to_arrow!(f64, arrays);
        let improvement_surcharge = to_arrow!(f64, arrays);
        let total_amount = to_arrow!(f64, arrays);

        Self {
            len,
            vendor_id,
            pickup_datetime,
            dropoff_datetime,
            passenger_count,
            trip_distance,
            ratecode_id,
            store_and_fwd_flag,
            pu_location_ic,
            do_location_ic,
            payment_type,
            fare_amount,
            extra,
            mta_tax,
            tip_amount,
            tolls_amount,
            improvement_surcharge,
            total_amount,
        }
    }

    pub fn to_arrays(self) -> Vec<Arc<dyn Array>> {
        vec![
            Arc::new(self.vendor_id),
            Arc::new(self.pickup_datetime),
            Arc::new(self.dropoff_datetime),
            Arc::new(self.passenger_count),
            Arc::new(self.trip_distance),
            Arc::new(self.ratecode_id),
            Arc::new(self.store_and_fwd_flag),
            Arc::new(self.pu_location_ic),
            Arc::new(self.do_location_ic),
            Arc::new(self.payment_type),
            Arc::new(self.fare_amount),
            Arc::new(self.extra),
            Arc::new(self.mta_tax),
            Arc::new(self.tip_amount),
            Arc::new(self.tolls_amount),
            Arc::new(self.improvement_surcharge),
            Arc::new(self.total_amount),
        ]
    }

    pub fn len(&self) -> usize {
        self.len
    }

    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }
}

impl From<TaxiArrow> for Chunk<Arc<dyn Array>> {
    fn from(val: TaxiArrow) -> Self {
        Chunk::new(val.to_arrays())
    }
}

pub struct TaxiArrowReader {
    reader: FileReader<File>,
}

impl TaxiArrowReader {
    /// Given that the provided path contains taxi data on parquet format,
    /// creates a object which can iterate over `batch_size` elements.
    ///
    /// # Errors
    ///
    /// This function will return an error if the given path does not contain a parquet file with at least 17 columns.
    pub fn new(conf: &Conf) -> std::io::Result<Self> {
        let path = Path::new(&conf.data_path);
        let file = File::open(path)?;
        let projection: Vec<usize> = (0..17).collect();
        let mut reader = FileReader::try_new(
            file,
            Some(&projection),
            Some(conf.batch_size),
            Some(conf.limit),
            None,
        )
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;
        debug!("{:#?}", reader.schema().fields);

        // Make sure the data is in memory before continuing.
        // Skipping the first element is fine as long as we always do it.
        // NOTE: we'll read one less element than expected from limit/batch-size!
        reader.next();
        Ok(Self { reader })
    }
}

impl Iterator for TaxiArrowReader {
    type Item = TaxiArrow;

    fn next(&mut self) -> Option<Self::Item> {
        let maybe_chunk = self.reader.next()?;
        let chunk = maybe_chunk.unwrap();
        Some(TaxiArrow::new(chunk))
    }
}

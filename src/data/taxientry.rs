use rkyv::{Archive, Deserialize, Serialize};
use rskafka::client::{partition::PartitionClient, ClientBuilder};
use tokio::runtime::Runtime;

use super::taxiarrow::TaxiArrow;

#[derive(Deserialize, Serialize, PartialEq, Debug, Archive, Default, Clone)]
pub struct TaxiEntry {
    pub vendor_id: Option<i64>,
    pub pickup_datetime: Option<i64>,
    pub dropoff_datetime: Option<i64>,
    pub passenger_count: Option<f64>,
    pub trip_distance: Option<f64>,
    pub ratecode_id: Option<f64>,
    pub store_and_fwd_flag: Option<String>,
    pub pu_location_ic: Option<i64>,
    pub do_location_ic: Option<i64>,
    pub payment_type: Option<i64>,
    pub fare_amount: Option<f64>,
    pub extra: Option<f64>,
    pub tip_amount: Option<f64>,
    pub tolls_amount: Option<f64>,
    pub improvement_surcharge: Option<f64>,
    pub total_amount: Option<f64>,
    pub mta_tax: Option<f64>,
}

macro_rules! from_arrow {
    ($array:expr, $num: expr) => {
        match $array.validity() {
            None => Some($array.value($num)),
            Some(bitmap) => match bitmap.get_bit($num) {
                true => None,
                false => Some($array.value($num)),
            },
        }
    };
}

impl From<TaxiArrow> for Vec<TaxiEntry> {
    fn from(ta: TaxiArrow) -> Self {
        let mut vec = Vec::with_capacity(ta.len());

        for i in 0..ta.len() {
            vec.push(TaxiEntry {
                vendor_id: from_arrow!(ta.vendor_id, i),
                pickup_datetime: from_arrow!(ta.pickup_datetime, i),
                dropoff_datetime: from_arrow!(ta.dropoff_datetime, i),
                passenger_count: from_arrow!(ta.passenger_count, i),
                trip_distance: from_arrow!(ta.trip_distance, i),
                ratecode_id: from_arrow!(ta.ratecode_id, i),
                store_and_fwd_flag: from_arrow!(ta.store_and_fwd_flag, i).map(|str| str.to_owned()),
                pu_location_ic: from_arrow!(ta.pu_location_ic, i),
                do_location_ic: from_arrow!(ta.do_location_ic, i),
                payment_type: from_arrow!(ta.payment_type, i),
                fare_amount: from_arrow!(ta.fare_amount, i),
                extra: from_arrow!(ta.extra, i),
                mta_tax: from_arrow!(ta.mta_tax, i),
                tip_amount: from_arrow!(ta.tip_amount, i),
                tolls_amount: from_arrow!(ta.tolls_amount, i),
                improvement_surcharge: from_arrow!(ta.improvement_surcharge, i),
                total_amount: from_arrow!(ta.total_amount, i),
            });
        }

        vec
    }
}

impl TaxiEntry {
    pub fn row_pipeline(_iter: &[Self]) -> (f64, u64) {
        todo!()
        // let passangers = Self::passanger_counter(iter);
        // let trip = iter.iter().filter_map(|a| a.trip_distance).sum();
        // (trip, passangers)
    }

    // fn passanger_counter(iter: &[Self]) -> u64 {
    //     iter.iter().filter_map(|a| a.passenger_count).sum()
    // }
}

/// A helper struct to get taxi records from Kafka.
/// The intention is to expose it as a efficient iterator to be used in benchmarks.
#[derive(Debug)]
pub struct TaxiIterator<'a> {
    offset: i64,
    high_watermark: i64,
    buffer: Vec<TaxiEntry>,
    partiton_client: PartitionClient,
    tokio_handle: &'a Runtime,
    batchsize: i32,
}

impl<'a> TaxiIterator<'a> {
    pub fn new(topic: &str, connection: &str, batchsize: i32, tokio_handle: &'a Runtime) -> Self {
        let offset = 0;
        let high_watermark = 0;
        let buffer = Vec::new();
        let client_spawn = ClientBuilder::new(vec![connection.to_owned()]).build();
        let client = tokio_handle.block_on(client_spawn).unwrap();
        let partiton_client = tokio_handle
            .block_on(client.partition_client(topic.to_owned(), 0))
            .unwrap();

        Self {
            offset,
            high_watermark,
            buffer,
            partiton_client,
            batchsize,
            tokio_handle,
        }
    }

    pub fn fill_buffer(&mut self) {
        let (records, high_watermark) = self
            .tokio_handle
            .block_on(
                self.partiton_client
                    .fetch_records(self.offset, 1..self.batchsize, 1000),
            )
            .unwrap();
        self.high_watermark = high_watermark;

        for rec in records {
            self.offset = rec.offset;
            let bytes = rec.record.value.unwrap();
            let archived = unsafe { rkyv::archived_root::<TaxiEntry>(&bytes) };
            let value: TaxiEntry = archived.deserialize(&mut rkyv::Infallible).unwrap();
            self.buffer.push(value);
        }
    }
}

impl<'a> Iterator for TaxiIterator<'a> {
    type Item = TaxiEntry;

    fn next(&mut self) -> Option<Self::Item> {
        if !self.buffer.is_empty() {
            return self.buffer.pop();
        }
        if self.high_watermark == self.offset {
            return None;
        }
        self.fill_buffer();
        self.buffer.pop()
    }
}

use anyhow::Result;

use futures::StreamExt;
use rkyv::Deserialize;
use std::{ops::AddAssign, sync::Arc, time::Instant};
use tokio::{
    io::Interest,
    net::{TcpListener, TcpStream},
    sync::Mutex,
};
use tokio_util::codec::{Framed, LengthDelimitedCodec};
use tracing::debug;

#[allow(unused_imports)]
use futures::sink::SinkExt;

use crate::{data::{conf::Conf, taxientry::TaxiEntry}, send_durration};

type Sum = Arc<Mutex<f64>>;

async fn process(conf: Arc<Conf>, stream: TcpStream, sum: Sum) -> Result<()> {
    let output_uri = conf.output_uri.to_owned();

    let start = Instant::now();
    let mut agg = 0.0;

    let dest = tokio::spawn(async move {
        let dest = output_uri.to_owned();
        let stream = TcpStream::connect(dest).await.expect(&format!(
            "No destination to send elapsed time were found on {}!",
            output_uri
        ));
        let codec = LengthDelimitedCodec::new();
        Framed::new(stream, codec)
    });

    stream.ready(Interest::READABLE).await?;
    let mut codec = LengthDelimitedCodec::new();
    codec.set_max_frame_length(usize::MAX);
    let mut transport = Framed::new(stream, codec);

    while let Some(res_bytes) = transport.next().await {
        match res_bytes {
            Ok(bytes) => {
                let archive = unsafe { rkyv::archived_root::<Vec<TaxiEntry>>(&bytes) };
                let data: Vec<TaxiEntry> = archive.deserialize(&mut rkyv::Infallible).unwrap();
                for te in data.iter() {
                    agg += te.extra.unwrap_or(0.0);
                    agg += te.improvement_surcharge.unwrap_or(0.0);
                    agg += te.tolls_amount.unwrap_or(0.0);
                    agg += te.fare_amount.unwrap_or(0.0);
                    agg += te.tip_amount.unwrap_or(0.0);
                    agg += te.mta_tax.unwrap_or(0.0);
                }
                debug!("read some entries: {:?}", data);
            }
            Err(_) => todo!("Not handling failure to read bytes from network buffer"),
        }
    }
    let mut total_sum = *sum.lock().await;
    total_sum.add_assign(agg);
    debug!("new total agg: {:?}", total_sum);

    // Send data to destination
    let mut final_transport = dest.await?;
    let data = rkyv::to_bytes::<_, 64>(&total_sum).unwrap().to_vec().into();
    final_transport.send(data).await?;

    // =send time to benchmark
    let durr = start.elapsed();
    send_durration(conf.as_ref(), durr).await?;
    Ok(())
}

pub async fn aggrows(conf: Conf) -> Result<()> {
    let listener = TcpListener::bind(&conf.input_uri).await?;
    let sum: Sum = Arc::new(Mutex::new(0.0));
    let conf = Arc::new(conf);

    debug!("Entering listening loop");
    loop {
        let sum = sum.clone();
        let (socket, _) = listener.accept().await?;
        let conf = conf.clone();

        tokio::spawn(async move {
            // Process each socket concurrently.
            process(conf, socket, sum).await
        });
    }
}

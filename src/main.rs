use aggrowgator::data::conf::Conf;
use anyhow::Result;
use clap::Parser;
use tracing::debug;

#[tokio::main(worker_threads = 1)]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();
    let conf = Conf::parse();

    debug!("recieved conf: {:?}", conf);

    aggrowgator::run(conf).await
}

use std::time::Instant;

use anyhow::Result;
use bytes::Bytes;
use futures::SinkExt;
use tokio::net::TcpStream;
use tokio_util::codec::{Framed, LengthDelimitedCodec};
use tracing::{debug, event_enabled, Level};

use crate::{
    data::{conf::Conf, taxiarrow::TaxiArrowReader, taxientry::TaxiEntry},
    send_durration,
};

pub async fn run(conf: &Conf) -> Result<()> {
    debug!("Runnning!");
    let aggrigator_uri = conf.output_uri.clone();
    let stream = TcpStream::connect(aggrigator_uri).await?;
    let mut batch_data: Vec<Bytes> = Vec::new();
    let mut batches: Vec<Vec<TaxiEntry>> = Vec::new();
    let mut codec = LengthDelimitedCodec::new();
    codec.set_max_frame_length(usize::MAX);

    for tarrow in TaxiArrowReader::new(conf).unwrap() {
        let batch: Vec<TaxiEntry> = tarrow.into();
        batches.push(batch);
    }

    // This is were the computations which should be benched start.
    let start = Instant::now();
    for batch in batches {
        let data = rkyv::to_bytes::<_, 1024>(&batch).unwrap().to_vec().into();
        // let data: Bytes = data;
        batch_data.push(data);
    }
    let mut transport = Framed::new(stream, codec);
    if event_enabled!(Level::DEBUG) {
        for (i, b) in batch_data.into_iter().enumerate() {
            debug!("Sending batch {i}");
            transport.send(b).await?;
        }
    } else {
        for b in batch_data.into_iter() {
            transport.send(b).await?;
        }
    }

    let durr = start.elapsed();
    debug!("Sending data took {:?}", durr);
    send_durration(conf, durr).await?;

    Ok(())
}

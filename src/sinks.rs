use anyhow::Result;
use tracing::{trace, instrument};
use crate::data::{sendable_durr::Durr, conf::Conf};
use futures::{stream::FuturesUnordered, StreamExt};
use rkyv::Deserialize;
use std::time::Duration;
use tokio::{
    io::Interest,
    net::{TcpListener, TcpStream},
    spawn,
};
use tokio_util::codec::{Framed, LengthDelimitedCodec};

pub async fn test_sinks(conf: &Conf) -> Result<()> {
    spawn(aggregate_sink(conf.output_uri.to_owned()));
    duration_sink(conf.time_uri.to_owned(), u64::MAX).await?;
    Ok(())
}

pub async fn aggregate_sink(uri: String) -> anyhow::Result<()> {
    let listener = TcpListener::bind(&uri).await?;
    loop {
        let (stream, _) = listener.accept().await?;
        stream.ready(Interest::READABLE).await?;
        let codec = LengthDelimitedCodec::new();
        let mut transport = Framed::new(stream, codec);
        transport.next().await;
    }
}
async fn get_duration(stream: TcpStream) -> anyhow::Result<Duration> {
    let codec = LengthDelimitedCodec::new();
    let mut transport = Framed::new(stream, codec);
    if let Some(Ok(bytes)) = transport.next().await {
        let archive = unsafe { rkyv::archived_root::<Durr>(&bytes) };
        let remote_durr: Durr = archive.deserialize(&mut rkyv::Infallible).unwrap();
        return Ok(remote_durr.durr);
    }
    panic!("Could not read time properly");
}

#[instrument(level = "debug")]
pub async fn duration_sink(uri: String, durrations: u64) -> anyhow::Result<Duration> {
    let mut duration = Duration::ZERO;
    let listener = TcpListener::bind(&uri).await?;
    let futures = FuturesUnordered::new();

    for i in 0..durrations {
        let (stream, _) = listener.accept().await?;
        stream.ready(Interest::READABLE).await?;
        let durr = get_duration(stream);
        futures.push(durr);
        trace!("Got duration {i}/{durrations}");
    }
    futures
        .collect::<Vec<anyhow::Result<Duration>>>()
        .await
        .into_iter()
        .for_each(|a| {
            duration += a.unwrap();
        });

    Ok(duration)
}

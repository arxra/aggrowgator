FROM --platform=linux/amd64 debian
COPY target/release/aggrowgator /usr/local/bin/myapp
CMD ["myapp"]

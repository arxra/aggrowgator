use std::process::Command;

use aggrowgator::{
    data::conf::{Conf, Purpose},
    sinks::duration_sink,
};
use criterion::{
    criterion_group, criterion_main, AxisScale, BenchmarkId, Criterion, PlotConfiguration,
    Throughput,
};

mod profiler;

const LIMIT: u32 = 10;
const ELEMS: usize = 2_usize.pow(LIMIT);

fn build_taxi_bench(c: &mut Criterion) {
    // For debugging, one can use the tracing crate.
    // tracing_subscriber::fmt().init();

    let mut group = c.benchmark_group("aggregations");
    let config = PlotConfiguration::default().summary_scale(AxisScale::Logarithmic);
    group.plot_config(config);

    // We'll create our own tokio instance instead of handing it to criterion for no reason
    let tokio = tokio::runtime::Runtime::new().unwrap();

    let aggrigator_uri = "127.0.0.1:9094".to_owned();
    let sum_sink_uri = "127.0.0.1:9095".to_owned();
    let time_uri = "127.0.0.1:9096".to_owned();
    let path = "/Users/aronhansenberggren/Downloads/yellow_tripdata_2022-03.parquet".to_owned();
    let my_topic = "my_topic".to_owned();

    let mut conf = Conf::new(
        Purpose::Runner,
        aggrigator_uri.clone(),
        sum_sink_uri.clone(),
        time_uri.clone(),
        my_topic,
        false,
        path.clone(),
        10,
        ELEMS as usize,
    );

    let args_agg = [
        "-p",
        "agg",
        "-o",
        &sum_sink_uri,
        "-i",
        &aggrigator_uri.to_owned(),
        "--time-uri",
        &time_uri,
    ];
    let args_run = [
        "-p",
        "runner",
        "-o",
        &aggrigator_uri.to_owned(),
        "--time-uri",
        &time_uri,
        "-d",
        &path.to_owned(),
    ];

    for size in (8..=LIMIT).step_by(2).map(|a| 2_usize.pow(a)) {
        group.throughput(Throughput::Elements(ELEMS as u64));
        group.bench_with_input(BenchmarkId::new("Rows", size), &size, |b, &_size| {
            b.iter_custom(|iters| {
                // Update the conf with new settings
                conf.set_batch_size(size);

                // we need somewhere to send the end products to be ignored
                tokio.spawn(aggrowgator::sinks::aggregate_sink(sum_sink_uri.clone()));

                // Spawn the aggregator before the runner
                let mut aggrigate_child = Command::new("./target/release/aggrowgator")
                    .args(args_agg)
                    .spawn()
                    .unwrap();
                println!("Running aggregator as pid {}", aggrigate_child.id());

                // Spawn a socket to listen for durrations of the remote runs
                let duration_fut = tokio.spawn(duration_sink(time_uri.clone(), iters * 2));

                // Let the runner go to completion
                let mut runner_child = Command::new("./target/release/aggrowgator")
                    .args(args_run)
                    .spawn()
                    .unwrap();

                println!("Running runner as pid {}", runner_child.id());

                let duration = tokio.block_on(duration_fut).unwrap().unwrap();

                // cleanup and return
                aggrigate_child.kill().unwrap();
                runner_child.kill().unwrap();

                duration
            });
        });
        // group.bench_with_input(BenchmarkId::new("Arrow", size), &size, |b, &_size| {
        //     b.iter_custom(|iters| {
        //         let mut durr = Duration::ZERO;

        //         for _i in 0..iters {
        //             let mut elems = TaxiIterator::new(topic, connection, batchsize, &tokio_handle);
        //             elems.fill_buffer();
        //             let start = Instant::now();

        //             // Test
        //             let mut arrow_taxi_builder = ArrowBuilderTaxi::with_capacity(size);

        //             for e in elems.take(size) {
        //                 arrow_taxi_builder.add_taxi(e);
        //             }
        //             let taxis = arrow_taxi_builder.finalize();
        //             black_box(taxis);

        //             // cleanup
        //             let iter_durr = start.elapsed();
        //             durr += iter_durr;
        //         }
        //         durr
        //     });
        // });
    }
    group.finish();
}

criterion_group! {
    name = benches;
    config = Criterion::default().with_profiler(profiler::FlamegraphProfiler::new(100));
    targets = build_taxi_bench
}
criterion_main!(benches);
